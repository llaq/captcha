# encoding: utf-8

import os

import config
from app import admin, db
from app.model.model import Model, View


class VisualSource(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Path of source image used for building some challenges
    filename = db.Column(db.String(1000))
    # Author of source image
    author = db.Column(db.String(1000))
    # Licence of source image
    license = db.Column(db.String(1000))

    @property
    def filepath(self):
        return os.path.join(
            config.VISUAL_SOURCE_DIR,
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )

    @property
    def fileurl(self):
        if self.filename.startswith(config.TEST_TOKEN):
            return os.path.join(
                "test/visual/sources",
                self.filename[:2],
                self.filename[2:4],
                self.filename[4:6],
                self.filename,
            )
        return os.path.join(
            config.VISUAL_SOURCE_URL,
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )


# Model will be automatically managed through flask-admin module
admin.add_view(View(VisualSource, db.session))
