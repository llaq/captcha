# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Solution(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Public key associated to challenge
    public = db.Column(db.String(40))
    # Domain name associated to challenge
    domain = db.Column(db.String(1000))
    # Token associated to solution
    token = db.Column(db.String(64))
    # Value associated to visual solution
    visual = db.Column(db.String(64))
    # Value associated to audio solution
    audio = db.Column(db.String(100))
    # Date and time solution has been created
    date = db.Column(db.DateTime)


# Model will be automatically managed through flask-admin module
admin.add_view(View(Solution, db.session))
